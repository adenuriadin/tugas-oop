<?php 
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("Shaun");
   echo "Animal Name : $sheep->name <br>";
   echo "Legs : $sheep->legs <br>";
   echo "Cold Blood : $sheep->cold_blood <br>";

$Ape = new Ape("Kera Sakti");
   echo "<br> Animal Name : $Ape->name <br>";
   echo "Legs : $Ape->legs <br>";
   echo "Cold Blood : $Ape->cold_blood <br>";
   echo $Ape->yell() . "<br>";

$Frog = new Frog("Buduk");
   echo "<br> Animal Name : $Frog->name <br>";
   echo "Legs : $Frog->legs <br>";
   echo "Cold Blood : $Frog->cold_blood <br>";
   echo $Frog->jump();
   